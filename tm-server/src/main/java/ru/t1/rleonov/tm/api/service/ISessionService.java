package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Session;

public interface ISessionService {

    @NotNull
    @SneakyThrows
    Session create(@Nullable Session session);

    Boolean existsById(@Nullable String id);

    @Nullable
    @SneakyThrows
    Session findOneById(@Nullable String id);

    @NotNull
    @SneakyThrows
    Session removeById(@Nullable String id);

}
