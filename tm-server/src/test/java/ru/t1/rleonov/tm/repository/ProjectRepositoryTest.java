package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Project;
import java.util.ArrayList;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    /*@Test
    public void create() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @Nullable final String name = USER1_PROJECT1.getName();
        @Nullable final String description = USER1_PROJECT1.getDescription();
        @Nullable final Project project = repository.create(user_id, name, description);
        Assert.assertEquals(user_id, project.getUserId());
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void createNoDesc() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @Nullable final String name = USER1_PROJECT1.getName();
        @Nullable final Project project = repository.create(user_id, name);
        Assert.assertEquals(user_id, project.getUserId());
        Assert.assertEquals(name, project.getName());
    }

    @Test
    public void getSize() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        Assert.assertEquals(ALL_PROJECTS.size(), repository.getSize());
    }

    @Test
    public void add() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(USER1_PROJECTS);
        repository.add(USER2_PROJECT1);
        Assert.assertEquals(USER2_PROJECT1, repository.findOneByIndex(2));
    }

    @Test
    public void findAll() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        Assert.assertEquals(USER2_PROJECTS, repository.findAll(USER2.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final List<Project> sortedProjects = new ArrayList<>(ALL_PROJECTS);
        sortedProjects.sort(CreatedComparator.INSTANCE);
        repository.set(sortedProjects);
        Assert.assertEquals(USER2_PROJECTS, repository.findAll(USER2.getId(), CreatedComparator.INSTANCE));
    }

    @Test
    public void existsById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @NotNull final String project_id = USER1_PROJECT1.getId();
        Assert.assertNotNull(repository.findOneById(user_id, project_id));
    }

    @Test
    public void findOneById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @NotNull final String project_id = USER1_PROJECT1.getId();
        Assert.assertEquals(USER1_PROJECT1, repository.findOneById(user_id, project_id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, repository.findOneByIndex(user_id, index));
    }

    @Test
    public void remove() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        repository.remove(user_id, USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneByIndex(user_id, 0));
    }

    @Test
    public void removeById() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @NotNull final String project_id = USER1_PROJECT1.getId();
        repository.removeById(user_id, project_id);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneByIndex(user_id, 0));
    }

    @Test
    public void removeByIndex() {
        @NotNull final IProjectRepository repository = new ProjectRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.set(ALL_PROJECTS);
        @Nullable final String user_id = USER1_PROJECT1.getUserId();
        @NotNull final Integer index = USER1_PROJECTS.indexOf(USER1_PROJECT1);
        repository.removeByIndex(user_id, index);
        Assert.assertEquals(USER1_PROJECT2, repository.findOneByIndex(user_id, 0));
    }*/

}
